#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

bazel test --test_output=all --verbose_failures //...
