# Dependencies managment
pip-tools==6.5.1

# Linting
pylint==2.9.6
pylint-plugin-utils==0.6

# Code formatting
black==22.3.0

# Testing
pytest==6.2.5