import base64
import logging

import graphene
import requests
import shapely.geometry
import shapely.ops
from app.api.errors import GraphQLServiceError
from app.lib.gis import Projection
from app.lib.requaferti import requaferti_client
from app.lib.requaferti_types import (
    RequafertiComputePParams,
    RequafertiComputePResult,
    RequafertiGetSolResultType,
    PDFGeneratorInputType,
    PDFGeneratorOutputType,
    RequafertiPResultError,
    RequafertiPResultEstimation,
    ExportableResidues,
    Besoin_P,
    RequafertiBesoinPParams
)
from app.settings import config
from graphene.types.generic import GenericScalar
from graphene_pydantic_updated import PydanticObjectType, PydanticInputObjectType
from opentelemetry import trace

logger = logging.getLogger(__name__)


# OpenTelemetry
def tracer():
    return trace.get_tracer(__name__)


####

class AnimalCategoryInput(graphene.InputObjectType):
    id = graphene.Int()
    libelle = graphene.String(required=False)
    libelle_de = graphene.String(required=False)

class OrganicMatterTypeInput(graphene.InputObjectType):
    id = graphene.Int()
    type_eff_xid = graphene.Int()
    grp_ani_xid = graphene.Int()
    phosphore_total = graphene.String()
    eff_code = graphene.String()

class PassInput(graphene.InputObjectType):
    id = graphene.Float()
    animalCategory = graphene.Field(AnimalCategoryInput, required=False)
    organicMatterType = graphene.Field(OrganicMatterTypeInput, required=False)
    effluentQuantity = graphene.Int(required=False)
    P2O5Content = graphene.Float(required=True)
    applicationDate = graphene.String(required=True)

class PassageInput(graphene.InputObjectType):
    pass_type = graphene.Int(required=False)
    max = graphene.Int(required=False)
    passes = graphene.List(PassInput,required=False)

class SimplePassageInput(graphene.InputObjectType):
    pass_type = graphene.Int()
    passage = graphene.Int()

class RequafertiComputePInput(graphene.InputObjectType):
    ech_num_ana = graphene.String(required=True)
    ech_cp = graphene.Int()
    ech_geoid = graphene.List(graphene.Float)
    ech_x = graphene.Float()
    ech_y = graphene.Float()
    references_p_sol_xid = graphene.Int(required=True)
    ph = graphene.Float(required=True)
    phosphore = graphene.Float(required=True)
    cot = graphene.Float(required=True)
    prof_prelev = graphene.Float(required=True)
    cailloux = graphene.Float(required=True)
    type_cult_xid = graphene.Int(required=True)
    r_rendement = graphene.Int()
    r_rendement_paille = graphene.Int()
    export_res_prec_yn = graphene.Int(required=True)
    type_cult_prec_xid = graphene.Int()


class RequafertiComputePParamsInput(PydanticInputObjectType):
    class Meta:
        model = RequafertiComputePParams
        name = "requaferti_phosphate__compute_p_params"

    organicPassages = graphene.List(PassageInput)
    mineralPassages = graphene.List(PassageInput)

class RequafertiBesoinPParamsInput(PydanticInputObjectType):
    class Meta:
        model = RequafertiBesoinPParams
        name = 'requaferti_phosphate__besoin_p_params'


class RequafertiPResultErrorType(PydanticObjectType):
    class Meta:
        model = RequafertiPResultError
        name = "RequafertiPResultError"


class RequafertiPResultEstimation(PydanticObjectType):
    class Meta:
        model = RequafertiPResultEstimation
        name = "RequafertiPResultEstimation"


class RequafertiSoilResponseType(PydanticObjectType):
    class Meta:
        model = RequafertiGetSolResultType
        name = "requaferti_phosphate__soil_response"


class RequafertiComputePResultType(PydanticObjectType):
    class Meta:
        model = RequafertiComputePResult
        name = "requaferti_phosphate__compute_p_result"


class Query(graphene.ObjectType):
    requaferti_phosphate__healthz = graphene.String(default_value="ok")
    requaferti_phosphate__config = GenericScalar(
        description="Requaferti configuration, as received from Requasud"
    )  # a dict
    requaferti_phosphate__postal_code = graphene.Field(
        graphene.String,
        description="Postal code",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )

    requaferti_phosphate__requaferti_mapping = GenericScalar(
        description="Requaferti mapping",
    )

    requaferti_phosphate__agricultural_region = graphene.Field(
        graphene.String,
        description="Agricultural region",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
    )
    requaferti_phosphate__soil = graphene.Field(
        RequafertiSoilResponseType,
        description="Soil properties",
        geometry=graphene.Argument(
            GenericScalar,
            required=True,
            description="__geo_interface__ dict, on EPSG:4258",
        ),
        phosphore=graphene.Argument(graphene.Float),
        ph_kcl=graphene.Argument(graphene.Float),
        carbone=graphene.Argument(graphene.Float),
        prof_prelev=graphene.Argument(graphene.Float),
        cailloux=graphene.Argument(graphene.Float),
    )

    requaferti_phosphate__exportable_residues = graphene.List(
        ExportableResidues,
        description="Exportable residues"
    )

    requaferti_phosphate__besoin_p = graphene.List(
        Besoin_P,
        description="Besoin_P",
        params=graphene.Argument(RequafertiBesoinPParamsInput, required=True),
    )

    requaferti_phosphate__compute_p = graphene.Field(
        RequafertiComputePResultType,
        description="Compute nitrogen recommendation",
        params=graphene.Argument(RequafertiComputePParamsInput, required=True),
    )

    requaferti_phosphate__generate_results_pdf = graphene.NonNull(
        PDFGeneratorOutputType,
        description="Generate the pdf file for the Requaferti results",
        input=graphene.Argument(PDFGeneratorInputType, required=True),
    )

    requaferti_phosphate__organic_matter_passes = GenericScalar(
        description="Organic matter passess"
    )

    requaferti_phosphate__mineral_matter_passes = GenericScalar(
        description="Fertilizer passess"
    )

    def resolve_requaferti_phosphate__requaferti_mapping(self, info):
        return requaferti_client.temp_mapping_opw_rqfs()

    async def resolve_requaferti_phosphate__exportable_residues(self, info):
        try:
            config = await requaferti_client.get_config()
            return config.get("residus_exportables") or None
        except Exception as exc:
            logger.exception("Failed to get the configuration!")
            raise GraphQLServiceError() from exc

    async def resolve_requaferti_phosphate__besoin_p(self, info, params):
        try:
            logger.debug(f"params: {params}")
            agricultural_region_id = params.agricultural_region_id
            rqs_cult_id = params.cult_id

            return await requaferti_client.get_besoin_p(agricultural_region_id, rqs_cult_id)


        except Exception as exc:
            logger.exception("Failed to get the configuration!")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_phosphate__config(self, info):
        try:
            return requaferti_client.get_config()
        except Exception as exc:
            logger.exception("Failed to get the configuration!")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_phosphate__postal_code(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            code_postal = requaferti_client.get_code_postal(geometry)
            return code_postal
        except Exception as exc:
            logger.exception("Failed to compute code postal")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_phosphate__agricultural_region(self, info, geometry):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__
            region_agricole = requaferti_client.get_region_agricole(geometry)
            return region_agricole
        except Exception as exc:
            logger.exception("Failed to compute region agricole")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_phosphate__soil(self, info, geometry, **kwargs):
        try:
            geometry = Projection.etrs89_to_belgian_lambert_72(
                shapely.geometry.shape(geometry)
            ).__geo_interface__

            sol = requaferti_client.get_sol(geometry, **kwargs)
            return sol
        except Exception as exc:
            logger.exception("Failed to get sol")
            raise GraphQLServiceError() from exc

    async def resolve_requaferti_phosphate__compute_p(self, info, params):
        try:
            params_dict = RequafertiComputePParams(**params)
            result = await requaferti_client.compute_p(params=params_dict)
        except Exception as exc:
            logger.exception("Failed to compute phosphate")
            raise GraphQLServiceError() from exc

        return result

    def resolve_requaferti_phosphate__generate_results_pdf(self, info, input):

        inputs = input["inputs"]

        try:
            # Get algorithm logo as static image
            with open(config.APP_DIR / "static/logo_requaferti.png", "rb") as f:
                logo = f.read()
                inputs["algorithm_logo_base64"] = (
                        "data:image/png;base64," + base64.b64encode(logo).decode()
                )
        except Exception as exc:
            logger.exception("Failed to read algorithm logo")
            raise GraphQLServiceError() from exc

        try:
            language = info.context["request"].headers["x-hasura-language"]

        except:
            logger.exception("Could not retrieve language from info, will use default template")
            language = 'fr'

        with tracer().start_as_current_span("generate_pdf"):
            try:
                template_file = "base_de.html" if language == 'de' else 'base.html'
                template_path = "templates/pdf/" + template_file
                with open(config.APP_DIR / template_path, "r") as f:
                    template = f.read()
                response = requests.post(
                    config.PDF_GENERATOR_ENDPOINT,
                    headers={},
                    json={"template": template, "data": inputs},
                )
                response.raise_for_status()

                return PDFGeneratorOutputType(pdf=response.content)

            except Exception as exc:
                logger.exception("Failed to build pdf")
                raise GraphQLServiceError() from exc

    requaferti_phosphate__dico_list = GenericScalar(
        description="Requaferti phosphate list configuration"
    )  # a dict

    #
    def resolve_requaferti_phosphate__dico_list(self, info):
        try:
            return requaferti_client.get_list()
        except Exception as exc:
            logger.exception("Failed to get the configuration!")
            raise GraphQLServiceError() from exc

    async def resolve_requaferti_phosphate__exploitation_mode(self, info):
        try:
            return requaferti_client.get_exploitation_mode()
        except Exception as exc:
            logger.exception("Failed to get the ugb !")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_phosphate__organic_matter_passes(self, info):
        try:
            return [{
                "pass_type": 1,
                "max": 3,
                "passes": []
            }]
        except Exception as exc:
            logger.exception("Failed to get the organic matter passes !")
            raise GraphQLServiceError() from exc

    def resolve_requaferti_phosphate__mineral_matter_passes(self, info):
        try:
            return [{
                "pass_type": 1,
                "max": 3,
                "passes": []
            }]
        except Exception as exc:
            logger.exception("Failed to get the mineral matter passes !")
            raise GraphQLServiceError() from exc
