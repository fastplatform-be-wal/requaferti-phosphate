import logging
import uvicorn
import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles

import graphene
from starlette_graphene3 import GraphQLApp

from app.settings import config
from app.tracing import Tracing
from app.api.query import Query
from app.lib.requaferti import requaferti_client


port = int(os.getenv('SERVICE_PORT', 7778))

logging.config.dictConfig(config.LOGGING_CONFIG)

# FastAPI
app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# GraphQL root
app.add_route(
    "/graphql",
    GraphQLApp(
        schema=graphene.Schema(query=Query, auto_camelcase=False)
    ),
)

# OpenTelemetry
Tracing.init(app)

# Log
logger = logging.getLogger(__name__)


@app.on_event("startup")
async def startup():
    requaferti_client.create_http_client()
    requaferti_client.load_geographic_data()


@app.on_event("shutdown")
async def shutdown():
    await requaferti_client.close_http_client()


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=port)