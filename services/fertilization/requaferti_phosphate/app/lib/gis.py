from functools import partial

from pyproj import (
    Proj,
    Transformer,
)

from shapely import ops

from app.settings import config


def add_crs(geometry, epsg=config.EPSG_SRID_ETRS89):
    return {
        **geometry,
        "crs": {
            "type": "name",
            "properties": {"name": f"urn:ogc:def:crs:EPSG::{epsg}"},
        },
    }


def reproject_function(source_epsg, target_epsg):

    source = Proj(f"EPSG:{source_epsg}")
    target = Proj(f"EPSG:{target_epsg}")

    transform = Transformer.from_proj(source, target, always_xy=True).transform

    return partial(ops.transform, transform)


class Projection:

    # EPSG:4258 -> EPSG:3035
    etrs89_to_etrs89laea = reproject_function(
        config.EPSG_SRID_ETRS89, config.EPSG_SRID_ETRS89_LAEA
    )

    # EPSG:3035 -> EPSG:4258
    etrs89laea_to_etrs89 = reproject_function(
        config.EPSG_SRID_ETRS89_LAEA, config.EPSG_SRID_ETRS89
    )

    # EPSG:4258 -> EPSG:4236
    etrs89_to_wgs84 = reproject_function(
        config.EPSG_SRID_ETRS89, config.EPSG_SRID_WGS84
    )

    # EPSG:4236 -> EPSG:4258
    wgs84_to_etrs89 = reproject_function(
        config.EPSG_SRID_WGS84, config.EPSG_SRID_ETRS89
    )

    # EPSG:31370 -> EPSG:4258
    belgian_lambert_72_to_etrs89 = reproject_function(
        config.EPSG_SRID_BELGIAN_LAMBERT_72, config.EPSG_SRID_ETRS89
    )

    # EPSG:4258 -> EPSG:31370
    etrs89_to_belgian_lambert_72 = reproject_function(
        config.EPSG_SRID_ETRS89, config.EPSG_SRID_BELGIAN_LAMBERT_72
    )

    # EPSG:4313 -> EPSG:4258
    belge_72_to_etrs89 = reproject_function(
        config.EPSG_SRID_BELGE_72, config.EPSG_SRID_ETRS89
    )

    # EPSG:4258 -> EPSG:4313
    etrs89_to_belge_72 = reproject_function(
        config.EPSG_SRID_ETRS89, config.EPSG_SRID_BELGE_72
    )

    


