import asyncio
import csv
import logging
from datetime import datetime
from threading import Lock
from typing import Callable
from urllib.error import HTTPError

import cachetools
import fiona
import httpx
from app.lib.requaferti_types import (
    RequafertiComputePParams,
    RequafertiComputePResult,
    RequafertiGetSolResultType,
)
from app.settings import config
from dateutil import parser
from shapely.geometry import shape
from shapely.strtree import STRtree

logger = logging.getLogger(__name__)


class RequafertiClient:
    def __init__(self):
        self.http_client = None

        self.codes_postaux_tree = None
        self.codes_postaux_index = None
        self.regions_agricoles_tree = None
        self.regions_agricoles_index = None
        self.charge_caillouteuse_tree = None
        self.charge_caillouteuse_index = None
        self.parametres_sol_par_code_postal = None
        self.parametres_sol_par_region_agricole = None
        self.parametres_sol_par_defaut = None

        self.configuration_lock = asyncio.Lock()
        self.configuration = None
        self.configuration_updated_at = None

        self.compute_p_cache = cachetools.TTLCache(
            maxsize=config.REQUAFERTI_COMPUTE_P_CACHE_MAX_SIZE,
            ttl=config.REQUAFERTI_COMPUTE_P_CACHE_TTL,
        )
        self.compute_p_cache_lock = Lock()

    def create_http_client(self):
        self.http_client = httpx.AsyncClient()

    async def close_http_client(self):
        await self.http_client.aclose()

    def load_geographic_data(self):
        """Load and index the GIS layers from shapefiles

        - codes postaux
        - régions agricoles

        """
        logger.info("Loading geographic data in memory")

        logger.info("Loading codes postaux")
        with fiona.open(
                config.APP_DIR / "lib/codes_postaux/codes_postaux.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.codes_postaux_tree = STRtree(shapes)
            self.codes_postaux_index = dict((id(s), f) for s, f in zip(shapes, data))

        logger.info("Loading régions agricoles")
        with fiona.open(
                config.APP_DIR / "lib/regions_agricoles/regions_agricoles.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.regions_agricoles_tree = STRtree(shapes)
            self.regions_agricoles_index = dict(
                (id(s), f) for s, f in zip(shapes, data)
            )

        logger.info("Loading charge caillouteuse")
        with fiona.open(
                config.APP_DIR / "lib/sol/charge_caillouteuse/charge_caillouteuse.shp", "r"
        ) as data:
            shapes = [shape(f["geometry"]) for f in data]
            self.charge_caillouteuse_tree = STRtree(shapes)
            self.charge_caillouteuse_index = dict(
                (id(s), f) for s, f in zip(shapes, data)
            )

        logger.info("Loading parametres sol par code postal")
        with open(
                config.APP_DIR / "lib/sol/parametres_sol_par_code_postal.csv"
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            self.parametres_sol_par_code_postal = {}
            for row in reader:
                code_postal = row.pop("code_postal")
                self.parametres_sol_par_code_postal[str(code_postal)] = {
                    k: float(v) if v != ""else None for k, v in row.items()
                }

        logger.info("Loading parametres sol par région agricole")
        with open(
                config.APP_DIR / "lib/sol/parametres_sol_par_region_agricole.csv"
        ) as csvfile:
            reader = csv.DictReader(csvfile)
            self.parametres_sol_par_region_agricole = {}
            for row in reader:
                region_agricole = row.pop("region")
                self.parametres_sol_par_region_agricole[region_agricole] = {
                    k: float(v) if v != ""else None for k, v in row.items()
                }

        logger.info("Loading parametres sol par défaut")
        with open(config.APP_DIR / "lib/sol/parametres_sol_par_defaut.csv") as csvfile:
            reader = csv.DictReader(csvfile)
            row = next(reader)
            self.parametres_sol_par_defaut = {
                k: float(v) if v != ""else None for k, v in row.items()
            }

        logger.info("...Geographic parameters are ready...")


    async def get_config(self):
        """Loads the Requaferti configuration from the `liste` and `dico` Requasud web
        services

        The config is cached in memory for a duration of REQUAFERTI_CONFIG_CACHE_TTL
        seconds
        """
        logger.debug(f"self.configuration is None: {self.configuration is None}")
        logger.debug(f"self.configuration_updated_at is None: {self.configuration_updated_at is None}")

        if self.configuration is not None and (
                (datetime.now() - self.configuration_updated_at).total_seconds() <= config.REQUAFERTI_CONFIG_CACHE_TTL
        ):
            logger.info("Reading config from cache")
            return self.configuration

        logger.debug("Refreshing config from the Requaferti endpoint")
        # Lock the configuration for updating
        async with self.configuration_lock:
            try:
                logger.info(f"Retrieving config for dico list {config.REQUAFERTI_CONFIG_LISTE_DICOS}")
                if (config.REQUAFERTI_CONFIG_LISTE_DICOS and config.REQUAFERTI_CONFIG_LISTE_DICOS != "None"):
                    liste_dicos = config.REQUAFERTI_CONFIG_LISTE_DICOS.split(",")
                else:
                    logger.debug("Retrieve all availables dictionaries")
                    dicoResponse = await self.http_client.post(
                        config.REQUAFERTI_CONFIG_DICO_LIST_URL,
                        timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                        auth=(config.REQUAFERTI_USERNAME, config.REQUAFERTI_PASSWORD,),
                    )
                    logger.debug(f"Dico response: {dicoResponse}")
                    dicoArray = dicoResponse.json()["liste_dico"]
                    liste_dicos = [dico["dico_flag"] for dico in dicoArray]

                    # This semaphore will block the number of downloads under the max
                # concurrency
                semaphore = asyncio.Semaphore(config.REQUAFERTI_CONFIG_DICO_CONCURRENCY)
                configuration = {}

                async def download_dico(dico):
                    logger.debug("Refreshing config from the Requaferti endpoint: %s", dico)
                    async with semaphore:
                        logger.debug(f"Processing dico: {dico}")
                        response = await self.http_client.post(
                            config.REQUAFERTI_CONFIG_DICO_URL,
                            data={"dico_flag": dico},
                            timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                            auth=(
                                config.REQUAFERTI_USERNAME,
                                config.REQUAFERTI_PASSWORD,
                            ),
                        )

                        try:
                            response.raise_for_status()
                        except HTTPError as http_err:
                            raise Exception(f"Issue with dico: {dico}: {http_err}")

                        data = response.json()["dico"]
                        # Write to the config dict, under a lock to prevent concurrent
                        # writes
                        async with asyncio.Lock():
                            configuration[dico] = data

                    logger.debug(f"Completed download for dico: {dico}")

                download_dicos = [download_dico(dico) for dico in liste_dicos]
                await asyncio.gather(*download_dicos)

            except Exception as e:
                logger.error(f"Failed to refresh the configuration: {str(e)}")
                raise
            else:
                self.configuration = configuration
                self.configuration_updated_at = datetime.now()

        return self.configuration

    def get_code_postal(self, geometry):
        """Return the postal code of a geometry

        Args:
            geometry (dict): a __geo_interface__ dict on EPSG:31370

        This is computed on the basis of the codes_postaux/codes_postaux.shp
        file in the lib directory.

        Returns:
            str: the postal code
        """
        geometry = shape(geometry)
        code_postal_feature = self._get_max_intersection_from_geometry_RG(
            geometry, self.codes_postaux_index
        )
        return (
            code_postal_feature["properties"]["nouveau_PO"]
            if code_postal_feature is not None
            else None
        )

    def get_region_agricole(self, geometry):
        """Return the agricultural region of a geometry

        This is computed on the basis of the regions_agricoles/regions_agricoles.shp
        file in the lib directory.

        Args:
            geometry (dict): a __geo_interface__ dict on EPSG:31370

        Returns:
            str: the name of the agricultural region
        """
        geometry = shape(geometry)
        region_agricole_feature = self._get_max_intersection_from_geometry_RG(
            geometry, self.regions_agricoles_index
        )
        return (
            region_agricole_feature["properties"]["NOM"]
            if region_agricole_feature is not None
            else None
        )

    def get_charge_caillouteuse(self, geometry):
        geometry = shape(geometry)
        return self._get_avg_field_intersection_from_geometry(
            geometry,
            self.charge_caillouteuse_tree,
            self.charge_caillouteuse_index,
            "CH_EST_SUR",
        )

    def _get_max_intersection_from_geometry_RG(self, geometry, index):

        for shape_id, feature in index.items():
            polygon_geometry = shape(feature["geometry"])
            if polygon_geometry.intersects(geometry):
                # Get the properties for the shape ID
                logger.debug(f"Properties for shape ID {shape_id}: {feature['properties']}")

                # Return the feature corresponding to the shape ID
                return feature

        # If no intersection is found, return None
        return None

    def _get_max_intersection_from_geometry(self, geometry, tree, index):
        """Return the feature from a layer that max intersects the provided geometry

        Args:
            geometry (shape): a Shapely geometry object
            tree (STRTree): the index tree as built from the source layer
            index (dict): an index mapping the memory id and a feature, as built from
                            the source layer

        Returns:
            geometry (dict): a __geo_interface__ dict on EPSG:31370
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [index[shape_id] for shape_id in shape_ids]

        # Find max intersection
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        index_intersection_area_max = max(
            range(len(intersection_areas)), key=lambda i: intersection_areas[i]
        )

        return features[index_intersection_area_max]

    def _get_avg_field_intersection_from_geometry(self, geometry, tree, index, field):
        """Return the average value of a field of all the features from a layer that 
        intersect the provided geometry

        Args:
            geometry (shape): a Shapely geometry object
            tree (STRTree): the index tree as built from the source layer
            index (dict): an index mapping the memory id and a feature, as built from
                            the source layer
            field (str): which field to use for the average

        Returns:
            geometry (dict): a __geo_interface__ dict on EPSG:31370
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [index[shape_id] for shape_id in shape_ids]

        # Find weighted averageof the field on the intersecting geometries
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        values = [float(f["properties"].get(field)) for f in features]

        # If all the intersecting geometries have no value, return None
        if all([v is None for v in values]):
            return None

        # If we are querying with a geometry that is a Point, then the intersection
        # areas will be zero for all intersections. In that case, we will just be
        # returning the average of the values.
        if all([i is None or i == 0 for i in intersection_areas]):
            intersection_areas = [1] * len(intersection_areas)

        # Return the average of the values of the field, weighted by the intersection
        # areas
        average = sum(
            [i * v for i, v in zip(intersection_areas, values) if v is not None]
        ) / sum([i for i, v in zip(intersection_areas, values) if v is not None])

        return average

    def get_sol_par_code_postal(self, code_postal):
        return self.parametres_sol_par_code_postal.get(str(code_postal), None)

    def get_sol_par_region_agricole(self, region_agricole):
        return self.parametres_sol_par_region_agricole.get(region_agricole, None)

    def get_sol_par_defaut(self):
        return self.parametres_sol_par_defaut

    async def get_besoin_p(self, agricultural_region_id, cult_name_id):
        config = await self.get_config()
        besoin_p = config.get("besoin_p") or None
        if besoin_p is None:
            raise Exception("Failed to get the besoin_p dico")

        # Filter the besoin_p list
        filtered_agricultural_region = [item for item in besoin_p if item.get("agr_id") == agricultural_region_id]
        filtered_besoin_p = [item for item in filtered_agricultural_region if item.get("type_cult_id") == cult_name_id]
        return filtered_besoin_p

    def get_sol(self, geometry, **kwargs):

        sol = RequafertiGetSolResultType()

        # First assign default values for whole Wallonia
        for k, v in requaferti_client.get_sol_par_defaut().items():
            if v is not None:
                setattr(sol, k, v)

        # Then override with values from the region agricole
        region_agricole = requaferti_client.get_region_agricole(geometry)
        if region_agricole is not None:
            sol_region_agricole = requaferti_client.get_sol_par_region_agricole(
                region_agricole
            )
            if sol_region_agricole is not None:
                for k, v in sol_region_agricole.items():
                    if v is not None:
                        setattr(sol, k, v)

        # Then override with values from the postal code
        code_postal = requaferti_client.get_code_postal(geometry)
        if code_postal is not None:
            sol_code_postal = requaferti_client.get_sol_par_code_postal(code_postal)
            if sol_code_postal is not None:
                for k, v in sol_code_postal.items():
                    if v is not None:
                        setattr(sol, k, v)

        # Then override with values from the charge_caillouteuse layers
        cailloux = requaferti_client.get_charge_caillouteuse(geometry)
        if cailloux is not None:
            cailloux = round(cailloux, 2)
            setattr(sol, "cailloux", cailloux)

        # Then override with values from the user (passed as arguments)
        for k, v in kwargs.items():
            if v is not None:
                setattr(sol, k, v)
        logger.debug(f"Returned value for {sol}")
        return sol

    def _process_parameters(self, query_params, passage_key: str, pass_handler: Callable):
        """
        Generic method to process passages and update query_params accordingly.

        Parameters:
        - query_params (dict): The input parameters dictionary.
        - passage_key (str): The key in query_params that holds the list of passages.
        - pass_handler (Callable): The function to handle processing of individual passes.

        Returns:
        None
        """
        passArray = query_params.pop(passage_key, None)
        if passArray is None:
            return

        result = {}  # Dictionary to store the final result
        passages = {i: None for i in range(1, 5)}  # Initialize a dictionary to store passages

        # Iterate over passages and store them in the dictionary
        for passage in passArray:
            pass_type = passage.get('pass_type')
            if 1 <= pass_type <= 4:
                passages[pass_type] = passage

        # Process each passage
        for i in range(1, 5):
            passage = passages[i]
            if passage:
                passes = passage.get('passes', [])
                if passes:
                    for j, pass_data in enumerate(passes):
                        pass_handler(i, pass_data, result)

        # Add result query to query_params
        query_params.update(result)

    def __parse_date(self, date):
        """
        Parses a given date string and converts it to the format YYYY-MM-DD.
        Parameters:
        - date (str): The input date string.
        Returns:
        - str: The date in YYYY-MM-DD format.
        """
        try:
            # Parse the date using dateutil.parser
            parsed_date = parser.parse(date)
            # Format the parsed date as YYYY-MM-DD
            return parsed_date.strftime('%Y-%m-%d')
        except (ValueError, TypeError):
            raise ValueError("Invalid date format. Please provide a valid date string.")


    def __organic_application_passes(self, i, pass_data, result):
        applicationDate = pass_data.get('applicationDate')
        if applicationDate is not None:
            parsed_date= self.__parse_date(applicationDate)
        result[f'date_app_app{i}'] = parsed_date

        result[f'cat_anim_app{i}'] = pass_data.get('animalCategory', {}).get('id')
        result[f'type_mo_app{i}'] = pass_data.get('organicMatterType', {}).get('type_eff_xid')
        result[f'quantite_eff_app{i}'] = pass_data.get('effluentQuantity')
        result[f'teneur_p_app{i}'] = pass_data.get('P2O5Content')


    def __mineral_application_passes(self, i, pass_data, result):
        applicationDate = pass_data.get('applicationDate')
        if applicationDate is not None:
            parsed_date = self.__parse_date(applicationDate)

        result[f'date_engrais_min_app{i}'] = parsed_date
        result[f'quantite_engrais_min_app{i}'] = pass_data.get('P2O5Content')

    def _process_parameters_pass_type(self, query_params):
        passArray = query_params.pop("type_passages")

        if passArray is None:
            return

        result = {}  # Dictionary to store the final result
        passages = {i: None for i in range(1, 6)}  # Initialize a dictionary to store passages

        # Iterate over passages and store them in the dictionary
        for passage in passArray:
            pass_type = passage['pass_type']
            if 1 <= pass_type <= 5:
                passages[pass_type] = passage

        # Process each passage
        for i in range(1, 5):
            passage = passages[i]
            if passage:
                result[f'type_passage_{i}'] = passage.get('passage', 0)
                result[f'engrais_min_{i}'] = 0
                result[f'teneur_p_min_{i}'] = 0
                result[f'r_dose_{i}'] = 0

        # Add result query to query_params
        for key, value in result.items():
            query_params[key] = value

    async def compute_p(self, params: RequafertiComputePParams):
        """Computes the N recommendation from Requaferti by caling the Requasud
        webservice

        A max of REQUAFERTI_COMPUTE_P_CACHE_MAX_SIZE results are cached in an TTLCache,
        and returned from there if existing.
        The results in the cache are expired after REQUAFERTI_COMPUTE_P_CACHE_TTL seconds.

        Args:
            params (RequafertiComputePParams): Input parameters

        Raises:
            GraphQLError: [description]

        Returns:
            RequafertiComputeNResult: Recommendation result
        """

        # Read and return from cachUniteGrosBetaile if entry exists
        with self.compute_p_cache_lock:
            result = self.compute_p_cache.get(str(params))

        if result is not None:
            logger.debug("compute_p: reading result from cache")
            return result

        # Otherwise, query from the API
        query_params = params.dict()

        # Destructuration of Passage array
        self._process_parameters(query_params,'organicPassages', self.__organic_application_passes)
        self._process_parameters(query_params,'mineralPassages', self.__mineral_application_passes)

        # Set default value if not set

        if query_params["ech_date"] is None:
            query_params['ech_date'] = "2000-01-01"

        if query_params["r_rendement"] is None:
            query_params['r_rendement'] = 0

        cp = self.get_code_postal_from_xy(query_params['ech_x'],query_params['ech_y'])
        query_params["ech_cp"] = cp or 5030

            # Convert Boolean params to 0 or 1 expected by the Requaferti API
        for param_name in query_params:
            if isinstance(query_params[param_name], bool):
                query_params[param_name] = 1 if query_params[param_name] else 0

        logger.debug(f"Params: {query_params}")
        response = await self.http_client.post(
            config.REQUAFERTI_COMPUTE_P_URL,
            data=query_params,
            timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
            auth=(config.REQUAFERTI_USERNAME, config.REQUAFERTI_PASSWORD),
            headers={"Accept-Encoding": "identity"},
        )
        logger.debug(f'Response {response.json()}')
        response_data = response.json()

        # ### This code portion is used to solved the issue with apollo client
        # ### The if no id is defined then the value are overwritten in the cache and the final result is wrong.
        # i = 1
        # for item in response_data['r_estimation']:
        #     item['id'] = 'dummy_' + str(i)
        #     i += 1
        # ###

        response.raise_for_status()

        # Parse the API result
        result = RequafertiComputePResult(**response_data)

        # Set the value in cache
        with self.compute_p_cache_lock:
            self.compute_p_cache[str(params)] = result

        # Return the result as a pydantic object
        return result

    def find_element_by_property(self, input_list, property_name, property_value):
        for element in input_list:
            if element.get(property_name) == property_value:
                return element
        return None

    async def get_list(self):
        try:
            response = await self.http_client.post(
                config.REQUAFERTI_CONFIG_DICO_LIST_URL,
                timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                auth=(
                    config.REQUAFERTI_USERNAME,
                    config.REQUAFERTI_PASSWORD,
                ),
            )
            response.raise_for_status()

            data = response.json()
            return data
        except Exception:
            logger.exception("Failed to retrieve the dico_list")
            raise

    async def get_requasud_code_postal(self, geometry):
        logger.debug("get_requasud_code_postal")
        try:
            config = await self.get_config()
            logger.debug(f"config retrieved")
            postal_code = self.get_code_postal(geometry=geometry)
            logger.debug(f"postal code retrieved: {postal_code}")
            return self.find_element_by_property(config["code_postaux"], "cp_code", postal_code)
        except Exception:
            logger.error("Failed to refresh the for postal code")
            raise

    async def get_leguminous_contribution(self, pasture_type=None):
        try:
            config = await self.get_config()
            contributionArray = config["prairie_fourniture_legu"]
            if pasture_type is None:
                return contributionArray

            return [c for c in contributionArray if c["prairie_type_id"] == pasture_type]
        except Exception:
            logger.error("Failed to refresh the configuration")
            raise

    async def get_exploitation_mode(self):
        try:
            dico = "prairie_type_mode_exploitation"
            response = await self.http_client.post(
                config.REQUAFERTI_CONFIG_DICO_URL,
                data={"dico_flag": dico},
                timeout=config.REQUAFERTI_DEFAULT_TIMEOUT,
                auth=(
                    config.REQUAFERTI_USERNAME,
                    config.REQUAFERTI_PASSWORD,
                ),
            )
            response.raise_for_status()

            data = response.json()["dico"]
            return data
        except Exception:
            logger.exception("Failed to refresh the configuration")
            raise

    def create_geometry_dict(self,x, y):
        """Create a GeoJSON-like dictionary for a point."""
        geometry = {
            "type": "Point",
            "coordinates": [x, y]
        }
        return geometry

    def get_code_postal_from_xy(self, x, y):
        """Return the postal code of a geometry based on coordinates.

        Args:
            x (float): The x-coordinate (longitude).
            y (float): The y-coordinate (latitude).

        Returns:
            str: the postal code
        """
        geometry = self.create_geometry_dict(x, y)
        shape_geom = shape(geometry)
        code_postal_feature = self._get_max_intersection_from_geometry_RG(
            shape_geom, self.codes_postaux_index
        )

        return (
            code_postal_feature["properties"]["nouveau_PO"]
            if code_postal_feature is not None
            else None
        )


def normalize(text):
    if text:
        return text.replace("’", "'").strip()
    return text.strip()


requaferti_client = RequafertiClient()
