import logging
from typing import List, Optional, Union

import graphene
from pydantic import BaseModel, Field
from pydantic import ValidationError, root_validator

logger = logging.getLogger(__name__)


def check_all_or_none(values, fields):
    values_has_fields = [f is not None in values for f in fields]
    if not all(values_has_fields) and any(values_has_fields):
        raise ValidationError(
            f"All the following fields should be filled (or none): {fields}"
        )


class AnimalCategory(BaseModel):
    id: int
    libelle: Optional[str]
    libelle_de: Optional[str]

class OrganicMatterType(BaseModel):
    id: int
    type_eff_xid: int
    type_eff_code: Optional[str]
    grp_ani_xid: int
    phosphore_total: str

class Pass(BaseModel):
    id: int
    animalCategory: Optional[AnimalCategory]
    organicMatterType: Optional[OrganicMatterType]
    effluentQuantity: Optional[int]
    P2O5Content: float
    applicationDate: str

class Passage(BaseModel):
    pass_type: int
    max: int
    passes: List[Pass]

    @root_validator
    @classmethod
    def check_pass_type(cls, values):
        if values["pass_type"] is None:
            raise ValidationError("pass_type must be provided")
        return values

class RequafertiComputePParams(BaseModel):
    organicPassages: Optional[List[Passage]]
    mineralPassages: Optional[List[Passage]]
    ech_num_ana: str
    ech_cp: Optional[int]
    ech_geoid: Optional[str]
    ech_x: Optional[float]
    ech_y: Optional[float]
    ech_date: Optional[str]
    references_p_sol_xid: int
    ph: float
    phosphore: float
    cot: float
    prof_prelev: float
    cailloux: float
    type_cult_xid: int
    r_rendement: Optional[int]
    r_rendement_paille: Optional[int]
    export_res_prec_yn: int
    type_cult_prec_xid: Optional[int]

    @root_validator
    @classmethod
    def check_ech(cls, values):
        if values["ech_cp"] is None and (values["ech_x"] is None or values["ech_y"] is None):
            raise ValidationError("ech_cp or ech_x and ech_y must be provided")
        return values

    @classmethod
    def to_input(cls, instance):
        return instance.dict()

    @classmethod
    def from_input(cls, input):
        return RequafertiComputePParams(**input)


class RequafertiPResultError(BaseModel):
    r_fourn_sol_erreur: Optional[str]
    r_apport_total_n_erreur: Optional[str]
    r_complement_n_erreur: Optional[str]


class RequafertiPResultEstimation(BaseModel):
    id: str
    r_dose: Optional[int]
    r_conseil_theorique: Optional[int]
    r_complement_n_erreur: Optional[str]

    @root_validator
    def debug_validation(cls, values):
        return values

    def dict(self, **kwargs):
        return super().dict(exclude_unset=True, **kwargs)


class RequafertiComputePResult(BaseModel):
    r_p1: Optional[float]
    r_da: Optional[float]
    r_tf: Optional[float]
    r_p2: Optional[float]
    r_besoins: Optional[float]
    r_besoins_paille: Optional[float]
    r_ep: Optional[float]
    r_ep_paille: Optional[float]
    r_exportation: Optional[float]
    r_exportation_paille: Optional[float]
    r_apports: Optional[float]
    r_stock_p: Optional[float]
    r_warning_stock_faible: Optional[int]
    r_export_res_prec: Optional[float]
    r_apport_orga_1: Optional[float]
    r_apport_orga_2: Optional[float]
    r_apport_orga_3: Optional[float]
    r_apport_orga: Optional[float]
    r_apport_min: Optional[float]
    r_apports_plaf: Optional[float]
    error: Optional[str]


class RequafertiGetSolResultType(BaseModel):
    phosphore: Optional[float] = Field(description='Phosphore mg/100g')
    ph_kcl: Optional[float] = Field(description='pH chlorure de potassium g/Kg')
    carbone: Optional[float] = Field(description='Teneur en carbone (%)')
    prof_prelev: Optional[float] = Field(description='Profondeur de prélévement cm')
    cailloux: Optional[float] = Field(description='Charge caillouteuse (%)')


class PDFGeneratorInputType(graphene.InputObjectType):
    inputs = graphene.JSONString(required=True)

    class Meta:
        name = "requaferti__pdf_generator_input"


class PDFGeneratorOutputType(graphene.ObjectType):
    pdf = graphene.Base64()

    class Meta:
        name = "requaferti__pdf_generator_output"


class PrairieFournitureLeguType(graphene.ObjectType):
    id = graphene.String()
    prairie_type_id = graphene.String()
    prairie_type_libelle = graphene.String()
    prairie_type_libelle_de = graphene.String()
    prairie_taux_trefle_id = graphene.String()
    taux_trefle_libelle = graphene.String()


class CloverCoveragePictures(graphene.ObjectType):
    name = graphene.String()
    url = graphene.String()


class ExportableResidues(graphene.ObjectType):
    id = graphene.String()
    type_cult_xid = graphene.String()
    type_recolte_xid = graphene.String()
    residu = graphene.String()
    residus_exportables = graphene.String()
    libelle_cult = graphene.String()
    libelle_cult_de = graphene.String()
    libelle_recolte = graphene.String()
    libelle_recolte_de = graphene.String()


class Besoin_P(graphene.ObjectType):
    id = graphene.String()
    type_cult_id = graphene.String()
    libelle_cult = graphene.String()
    libelle_recolte = graphene.String()
    agr_id = graphene.String()
    agr_libelle = graphene.String()
    rendement = graphene.String()
    exportation_p2o5 = graphene.String()
    exportation_k2O = graphene.String()
    exportation_mgO = graphene.String()
    besoin_p2o5 = graphene.String()


class RequafertiBesoinPParams(BaseModel):
    agricultural_region_id: str = Field(description="Agricultural region id")
    cult_id: str = Field(description="Crop Requasud ID")
