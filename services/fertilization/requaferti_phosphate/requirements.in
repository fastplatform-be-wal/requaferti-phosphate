# Server
uvicorn>=0.14.0
fastapi==0.66.0
pydantic==1.8.2
aiofiles

# HTTP client
httpx==0.18.2
requests>=2.23.0

# In-memory caching
cachetools==4.2.2

# GraphQL
graphene==3.0b7
graphql-core==3.1.7  # Fixed version to avoid regression in 3.2.0
starlette_graphene3==0.5.1
# TODO: change this to graphene-pydantic==0.2.0 once support for pydantic==1.8.2 is released
graphene-pydantic-updated==0.1.0

# Date & time
python-dateutil==2.8.2
pytz==2021.1

# GIS
fiona==1.8.20
shapely==1.7.1
pyproj==3.2.1

# pkg_resources for opentelemetry
setuptools==50.3.0

# Telemetry
opentelemetry-exporter-zipkin==1.6.2
opentelemetry-instrumentation-fastapi==0.29b0
opentelemetry-propagator-b3==1.6.2
